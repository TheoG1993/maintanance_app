-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2018 at 03:34 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trh_maintainer`
--

-- --------------------------------------------------------

--
-- Table structure for table `equipment`
--

CREATE TABLE `equipment` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `station` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equiped_at` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_maintanance_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `maintanance_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `equipment`
--

INSERT INTO `equipment` (`id`, `description`, `category`, `station`, `label`, `equiped_at`, `last_maintanance_at`, `maintanance_date`, `status`) VALUES
(2, 'Necta Biology 2012', '1', 'Station 1', 'TRHC2', 'May 24, 2018', 'May 24, 2018', '2018-06-14 16:04:17', 1),
(3, 'Reproduction 1', '1', 'Station 2', 'TRHC3', 'May 24, 2018', NULL, '2018-06-14 10:05:00', 1),
(7, 'Fire double', '2', 'Site 5', 'TRHFIRE7', 'May 24, 2018', NULL, '2019-05-24 10:18:47', 0),
(8, 'Samsung', '4', 'Site 2', 'TRHAc8', 'May 24, 2018', NULL, '2018-11-24 10:43:31', 1),
(9, 'Reproduction 1', '3', 'Station 1', 'TRHKIT9', 'May 24, 2018', NULL, '2020-05-24 10:44:29', 1),
(10, 'Reproduction 1', '1', 'Site 1', 'TRHCP10', 'May 24, 2018', NULL, '2018-06-14 13:15:24', 0),
(11, 'Necta Biology 2012', '1', 'Site 1', 'TRHCP11', 'May 24, 2018', NULL, '2018-06-14 13:15:40', 0),
(12, 'Arsenal', '1', 'Site 6', 'TRHCP12', 'May 24, 2018', 'May 24, 2018', '2018-06-14 16:02:24', 1),
(13, 'Necta 2001', '1', 'Site 1', 'TRHCP13', 'May 24, 2018', NULL, '2018-06-14 13:17:37', 0),
(14, 'Fire double dlld', '2', 'Site 5', 'TRHFIRE14', 'May 24, 2018', NULL, '2019-05-24 14:48:31', 0),
(15, 'Santana', '2', 'Site 1', 'TRHFIRE15', 'May 24, 2018', NULL, '2019-05-24 14:50:47', 0),
(16, 'Buble CD', '2', 'Site 7', 'TRHFIRE16', 'May 24, 2018', NULL, '2019-05-24 14:51:14', 1),
(17, 'Kit 35', '3', 'Site 6', 'TRHKIT17', 'May 24, 2018', NULL, '2020-05-24 14:58:00', 0),
(18, 'Samsung', '4', 'Site 2', 'TRHAc18', 'May 24, 2018', NULL, '2018-11-24 15:04:52', 1),
(19, 'Huawei Cooler extra', '4', 'Site 1', 'TRHAc19', 'May 24, 2018', NULL, '2018-11-24 15:05:41', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(8, '2018_05_23_164933_create_equipment_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Theodory Faustine', 'theodoryf@gmail.com', '$2y$10$MXDI8JRcq.20NiT5lkXmf.AvNLJ5pWplc6cJzZME7oja18uoMMQ7a', 'RziQ5pFUu1cPGOC6stuXaOL4GJzUliwxOqkC3pBTiOFIf9kTn1jao913CcJ3', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `equipment`
--
ALTER TABLE `equipment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
