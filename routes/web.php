<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Authentication.login');
})->name('/');

Route::get('/dashboard', 'equipmentsController@index')->middleware('auth')->name('/dashboard');

Route::post('/login_processing', 'userController@login')->name('/login_processing');
Route::get('/logout', 'userController@logout');

Route::get('/computers', 'equipmentsController@computer_printer')->name('/computers');

Route::get('/fire_extinguishers', 'equipmentsController@fire_ext')->name('/fire_extinguishers');

Route::get('/first_aid_kits', 'equipmentsController@first_kit')->name('/first_aid_kits');

Route::get('/air_conditions', 'equipmentsController@ac_dashboard')->name('/air_conditions');

//processing
Route::post('/add_equipment', 'equipmentsController@store');
Route::post('/edit', 'equipmentsController@edit');
Route::get('remove/{id}', 'equipmentsController@destroy');
Route::get('/update_status/{id}', 'equipmentsController@update_status');

//Report
Route::get('/generate_report', 'reportController@index');
Route::post('/filter', 'reportController@filter');
