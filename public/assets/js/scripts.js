$(document).ready(function(){
		$("#equipment").click(function(){

		$("#equipmentModal").modal("show");

		$("#submit").click( function( e ) {
		e.preventDefault();
		e.stopImmediatePropagation();

		var description = $("input[name='description']").val();
		var category= $("#category").val();
		var station = $("#station").val();
		
		if(description == ""){
			swal({
              title: "Sorry",

              text: "Sorry equipment description can't be empty",
              icon: "error",
	        });
	        $("#submit").text("Submit")
		}else{
			$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }),
    $.ajax( {
      url: 'http://45.33.52.54/maintanance_app/add_equipment',
      type: 'POST',
      data: {description: description, category: category, station: station},
      beforeSend: function(){

        $("#submit").text("Please wait....")
      },
      success: function(data){
      	if(data.message){
      		swal({
              title: "Success",

              text: ""+data.message,
              icon: "success",
	        });
      	}else if(data.error){
      		swal({
              title: "Sorry",
              text: ""+data.error,
              icon: "error",
            });
      	}
      	$("input[name='description']").val("");
      	$("#submit").text("Add Equipment");
      	$("#equipmentModal").modal("hide");

        if(data.category == "1"){

          window.location.href="http://45.33.52.54/maintanance_app/computers"
        }else if(data.category == "2"){
          window.location.href="http://45.33.52.54/maintanance_app/fire_extinguishers"
        }else if(data.category == "3"){
          window.location.href="http://45.33.52.54/maintanance_app/first_aid_kits"

        }else if(data.category == "4"){
           window.location.href="http://45.33.52.54/maintanance_app/air_conditions"

        }
      	
      }
    } );
		}
   
  } );
	})

    //edit equipment

       $("tbody #actions").find("#device").click(function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var id = $(this).attr('eqid');
        var description = $(this).attr('description');
        var category = $(this).attr('category');
        var station = $(this).attr('station');

       

        ///this will popup modal fro approval logics
        $("#description").val(description);
        $("#newcategory").val(category);
        $("#newstation").val(station);



        $("#equipmentEditModal").modal("show");


        $("#editEquipment").click(function(e){
            e.preventDefault();
            e.stopImmediatePropagation();

            var newdescription = $("#description").val();
            var newcategory= $("#newcategory").val();
            var newstation = $("#newstation").val();


    
    if(newdescription == "" && newcategory== "" && newstation== ""){
      swal({
              title: "Sorry",

              text: "Sorry equipment all fields are required",
              icon: "error",
          });
          $("#editEquipment").text("Submit")
          }else{
            $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              }),
          $.ajax( {
            url: 'http://45.33.52.54/maintanance_app/edit',
            type: 'POST',
            data: {id: id,description: newdescription, category: newcategory, station: newstation},
            beforeSend: function(){

              $("#editEquipment").text("Please wait....")
            },
            success: function(data){
              if(data.message){
                swal({
                    title: "Success",

                    text: ""+data.message,
                    icon: "success",
                });
              }else if(data.error){
                swal({
                    title: "Sorry",
                    text: ""+data.error,
                    icon: "error",
                  });
              }
              $("#editEquipment").text("Add Equipment");
              $("#equipmentEditModal").modal("hide");

              if(data.category == "1"){

                window.location.href="http://45.33.52.54/maintanance_app/computers"
              }else if(data.category == "2"){
                window.location.href="http://45.33.52.54/maintanance_app/fire_extinguishers"
              }else if(data.category == "3"){
                window.location.href="http://45.33.52.54/maintanance_app/first_aid_kits"

              }else if(data.category == "4"){
                 window.location.href="http://45.33.52.54/maintanance_app/air_conditions"

              }
              
            }
          } );
          }

            
        })






    


  
    })
})