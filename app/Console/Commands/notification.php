<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Auth;
use Mail;
use App\Equipment;
use App\Mail\notify;
use Carbon\carbon as Carbon;

class notification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:emailnotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Will Send Notification One week before';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $equipments = Equipment::where('status',0)->get();


        Mail::to('trhit@trhtz.com')
            ->cc('phpsoftware@trhtz.com')
            ->send(new notify($equipments));
   
        return 'Email was sent';

        
    }
}

