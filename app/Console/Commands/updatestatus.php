<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Auth;
use App\Equipment;
use Carbon\carbon as Carbon;

class updatestatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update equipment status one week before due date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $equipments = Equipment::where('status',1)->get();

        foreach ($equipments as $key => $equipment) {
            
            $now = Carbon::now();
            $maintanance_due_date = $equipment->maintanance_date;

            $days = $now->diffInDays($maintanance_due_date);

            if($days <= 7){
                
                Equipment::where('id',$equipment->id)->update(['status'=>0]);
            }
        }
    }
}
