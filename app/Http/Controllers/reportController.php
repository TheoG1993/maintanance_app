<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Equipment;
use Carbon\Carbon as Carbon;
use PDF;

class reportController extends Controller
{
      public function index()
    {
    	
    	return view('Report.dashboard')
    			;
    }

    //filter logics
      public function filter(Request $request)
    {
    	
    			$category = $request['category'];
    			$site = $request['site'];

    			if(!empty($category) && !empty($site)){
    				$equipments = Equipment::where('category',$category)
    						->where('station',$site)
    						->where('status',0)
    						->orderby('description','asc')
    						->get();

		    		$pdf = PDF::loadView('Report.document', ['equipments'=>$equipments]);


      			  return $pdf->download('Maintanance-items.pdf');

    			}else if(!empty($category) && empty($site)){

    				$equipments = Equipment::where('category',$category)
    						->where('status',0)
    						->orderby('description','asc')
    						->get();

		    		
    						
		    			    $pdf = PDF::loadView('Report.document', ['equipments'=>$equipments]);


      			 	 return $pdf->download('Maintanance-items.pdf');

    			}else if(empty($category) && !empty($site)){

    				$equipments = Equipment::where('station',$site)
    						->where('status',0)
    						->orderby('description','asc')
    						->get();

		    			$pdf = PDF::loadView('Report.document', ['equipments'=>$equipments]);


      			 		 return $pdf->download('Maintanance-items.pdf');
    			}
    }

}
