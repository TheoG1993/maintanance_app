<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class userController extends Controller
{
    public function index(){

    }

    //login process
    public function login(Request $request){

				$request->validate([
		    		'email'=> 'required|email|exists:users',
		    		'password' => 'required|min:8'
		    	]);

               

    	$email = $request['email'];
    	$password = $request['password'];

    	

    	if(Auth::attempt(['email'=> $email,'password' => $password])){

    		return redirect()->route('/dashboard');
    	}
    }

    //logout
    public function logout(){
    	Auth::logout();

    	return redirect()->route('/');
    }
}
