<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Equipment;
use Carbon\Carbon as Carbon;


class equipmentsController extends Controller
{
    public function index()
    {
    	$devices = Equipment::where('category','1')->count();
    	$fire_ext = Equipment::where('category','2')->count();
    	$kits = Equipment::where('category','3')->count();
    	$acs = Equipment::where('category','4')->count();

    	return view('Admin.dashboard')
    			->with('devices',$devices)
    			->with('fire_ext',$fire_ext)
    			->with('kits',$kits)
    			->with('acs',$acs);
    }

    //computer & printer function
    public function computer_printer()
    {
    	$devices = Equipment::where('category','1')
    						->orderby('status','asc')
    						->paginate(4);

    	return view('equipments.computer')
    			->with('devices',$devices);
    }

    //fire dashboard
    public function fire_ext()
    {
    	$devices = Equipment::where('category','2')
    						->orderby('status','asc')
    						->paginate(4);

    	return view('equipments.fire_extinguisher')
    			->with('devices',$devices);
    }

    //kit dashboard
    public function first_kit()
    {
    	$devices = Equipment::where('category','3')
    						->orderby('status','asc')
    						->paginate(4);

    	return view('equipments.first_aid')
    			->with('devices',$devices);
    }

    //ac dashboard
    public function ac_dashboard()
    {
    	$devices = Equipment::where('category','4')
    						->orderby('status','asc')
    						->paginate(4);

    	return view('equipments.ac')
    			->with('devices',$devices);
    }

    public function store(Request $request){
    	
    	$request->validate([
    		'description' => 'required',
    		'category' => 'required',
    		'station' => 'required'
    	]);

    	$description = $request['description'];
    	$category = $request['category'];
    	$station = $request['station'];

		$dt = Carbon::now();

		$now = $dt->toFormattedDateString();


    	$equipment = new Equipment();

    	

    	//for computers
    	if($category === "1"){

    		$equipment->description = $description;
    		$equipment->category = $category;
    		$equipment->station = $station;
    		$equipment->equiped_at = $now;
    		$equipment->maintanance_date = $dt->addWeeks(3);
    		$equipment->save();

    		$label = "TRHCP".$equipment->id;
    		$equipment->update(['label' => $label]);

    		$message = "Accessory Added Successifully";

    		return response()->json(['message'=>$message,'category'=>$category],200);

    	}else if($category === "2"){

    		$equipment->description = $description;
    		$equipment->category = $category;
    		$equipment->station = $station;
    		$equipment->equiped_at = $now;
    		$equipment->maintanance_date = $dt->addYear();
    		$equipment->save();

    		$label = "TRHFIRE".$equipment->id;
    		$equipment->update(['label' => $label]);

    		$message = "Fire Extinguisher Added Successifully";

    		return response()->json(['message'=>$message,'category'=>$category],200);

    	}else if($category === "3"){

    		$equipment->description = $description;
    		$equipment->category = $category;
    		$equipment->station = $station;
    		$equipment->equiped_at = $now;
    		$equipment->maintanance_date = $dt->addYears(2);
    		$equipment->save();

    		$label = "TRHKIT".$equipment->id;
    		$equipment->update(['label' => $label]);

    		$message = "First Aid Kit Added Successifully";

    		return response()->json(['message'=>$message,'category'=>$category],200);

    	}else if($category === "4"){

    		$equipment->description = $description;
    		$equipment->category = $category;
    		$equipment->station = $station;
    		$equipment->equiped_at = $now;
    		$equipment->maintanance_date = $dt->addMonths(6);

    		$equipment->save();

    		$label = "TRHAc".$equipment->id;
    		$equipment->update(['label' => $label]);

    		$message = "Ac Added Successifully";

    		return response()->json(['message'=>$message,'category'=>$category],200);

    	}
    }

    //edit
    public function edit(Request $request){
    	// dd($request->station);
    	$request->validate([
    		'description' => 'required',
    		'category' => 'required',
    		'station' => 'required'
    	]);

    	$description = $request['description'];
    	$category = $request['category'];
    	$station = $request['station'];
    	$id = $request['id'];

    	$array = array('description'=>$description,'category'=>$category,'station'=>$station);

    	Equipment::where('id',$id)->update($array);

    	$message = "Updated Successifully";

    	return response()->json(['message'=>$message,'category'=>$category],200);
    }

    //update status
     public function update_status($id){
        $dt = Carbon::now();

        $now = $dt->toFormattedDateString();

        $equip = Equipment::where('id',$id)->first();

        if($equip->category == "1"){

            $mdate = $dt->addWeeks(3);
          

        }else if($equip->category=="2"){
            $mdate = $dt->addYear();

        }else if($equip->category == "3"){
            $mdate = $dt->addYears(2);

        }else if($equip->category == "4"){
            $mdate = $dt->addMonths(6);
        }

        $array = array('status'=>1,'last_maintanance_at'=>$now,'maintanance_date'=>$mdate);

        Equipment::where('id',$id)->update($array);

        return redirect()->back();
    }

    //remove
    public function destroy($id){

    	Equipment::where('id',$id)->delete();

    	return redirect()->back();
    }
}
