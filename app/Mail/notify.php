<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Equipment;

class notify extends Mailable
{
    use Queueable, SerializesModels;

    protected $equipments;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($equipments)
    {
        $this->equipments = $equipments;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.notification')
                   ->with([
                  'equipments' => $this->equipments,
                ]) ;

    }
}
