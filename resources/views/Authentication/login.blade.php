<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>TRH - LOGIN</title>
        <meta content="MAINTANANCE TRUCKER" name="description" />
        <meta content="TRH LIMITED" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <link href="{{URL::to('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{URL::to('assets/css/icons.css')}}" rel="stylesheet" type="text/css">
        <link href="{{URL::to('assets/css/style.css')}}" rel="stylesheet" type="text/css">
        <style>
        	body{
        		overflow-x:hidden;overflow-y:auto;
        		background:url('http://45.33.52.54/maintanance_app/assets/images/cp_repair.jpg') no-repeat;
        		background-size: cover;
        	}
        </style>
</head>
<body>

        <!-- Begin page -->
        <div class="accountbg"></div>
        <div class="container-fluid">
        	<div class="topbar">

                        <nav class="navbar-custom">

                           

                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <h3 class="page-title">
                                    	<center>TANZANIA ROAD HAULAGE(1980) - MAINTANANCE TRUCKER</center>
                                    </h3>
                                </li>
                            </ul>

                            <div class="clearfix"></div>

                        </nav>

                    </div>
        </div>
        <div class="wrapper-page">

            <div class="card">
                <div class="card-block">

                    <h3 class="text-center mt-0 m-b-15">
                        <a href="" class="logo logo-admin"><img src="{{URL::asset('http://45.33.52.54/maintanance_app/assets/images/th.jpg')}}" height="80" alt="logo"></a>
                    </h3>

                    <h4 class="text-muted text-center font-18"><b>Sign In</b></h4>

                    @if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

                    <div class="p-3">
                        <form class="form-horizontal m-t-20" action="{{url('login_processing')}}" method="post">

                            <div class="form-group row">
                                <div class="col-12">
                                    <input class="form-control" type="email" name="email" placeholder="Email">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <input class="form-control" name="password" type="password" required placeholder="Password">
                                </div>
                            </div>


                            <div class="form-group text-center row m-t-20">
                                <div class="col-12">
                                    <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Log In</button>
                                </div>
                            </div>

                            <div class="form-group m-t-10 mb-0 row">
                                
                            </div>
                            @csrf
                        </form>
                    </div>

                </div>
            </div>
        </div>



        <!-- jQuery  -->
        <script src="{{URL::to('assets/js/jquery.min.js')}}"></script>
        <script src="{{URL::to('assets/js/tether.min.js')}}"></script>
        <!-- Tether for Bootstrap -->
        <script src="{{URL::to('assets/js/bootstrap.min.js')}}"></script>
        <script src="{{URL::to('assets/js/modernizr.min.js')}}"></script>
        <script src="{{URL::to('assets/js/detect.js')}}"></script>
        <script src="{{URL::to('assets/js/fastclick.js')}}"></script>
        <script src="{{URL::to('assets/js/jquery.slimscroll.js')}}"></script>
        <script src="{{URL::to('assets/js/jquery.blockUI.js')}}"></script>
        <script src="{{URL::to('assets/js/waves.js')}}"></script>
        <script src="{{URL::to('assets/js/jquery.nicescroll.js')}}"></script>
        <script src="{{URL::to('assets/js/jquery.scrollTo.min.js')}}"></script>

        <script src="{{URL::to('assets/pages/dashborad.js')}}"></script>

        <!-- App js -->
        <script src="{{URL::to('assets/js/app.js')}}"></script>
</body>
</html>