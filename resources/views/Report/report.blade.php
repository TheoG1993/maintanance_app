@extends('layouts.master')
@section('styles')

@endsection

@section('body')
    <div class="row">
        <div class="col-12">
            <div class="card m-b-20">
                <div class="card-block">

                    <h4 class="mt-0 header-title">
                        <form class="form-inline" action="{{url('filter/')}}" method="post">
                              <div class="form-group">
                                <label class="sr-only" for="category">Category</label>
                                <select class="form-control" name="category" id="category">
                                    <option value="1">Computer & Printers</option>
                                    <option value="2">Fire Extinguisher</option>
                                    <option value="3">First Aid Kit</option>
                                    <option value="4">Ac</option>
                                </select>
                              </div> &nbsp;&nbsp;&nbsp;
                              <div class="form-group">
                                <label class="sr-only" for="site">Site</label>
                                <select class="form-control" id="site" name="site">
                                            <option value="Site 1">Site 1</option>
                                                <option value="Site 2">Site 2</option>
                                                <option value="Site 3">Site 3</option>
                                                <option value="Site 5">Site 5</option>
                                                <option value="Site 6">Site 6</option>
                                                <option value="Site 7">Site 7</option>
                                                <option value="Site 8">Site 8</option>
                                                <option value="Site 9">Site 9</option>
                                                <option value="Site 10">Site 10</option>
                                </select>
                              </div>
                                &nbsp;&nbsp;&nbsp;
                              <button type="submit" class="btn btn-success">GENERATE</button>
                              @csrf
                            </form>
                    </h4>
                                     

                    <table class="table table-bordered table-hover order-table" style="width: 100%;">
                        <thead>
                        <tr>
                        
                            <th >Description</th>
                            <th>Label</th>
                            <th >Site</th>
                            <th >Last Maintanance</th>
                            <th >Due Date</th>

                           
                        </tr>
                        </thead>


                        <tbody>
                       
                        @foreach($devices as $key=>$device)
                           
                            <tr role="row" class="odd" style="">
                            
                            <td>{{$device->description}}</td>
                            <td>{{$device->label}}</td>
                            <td>{{$device->station}}</td>
                             @if($device->last_maintanance_at == "")
                                <td>No record</td>
                             @else
                                @php 
                                $ldate = Carbon\Carbon::parse($device->last_maintanance_at); 
                               @endphp 
                            <td>{{$ldate->format('M d Y')}}</td>
                             @endif

                            @php 
                                $mdate = Carbon\Carbon::parse($device->maintanance_date); 
                            @endphp 
                            <td>{{$mdate->format('M d Y')}}</td>
                        </tr>
                           
                        @endforeach

                    </tbody>

                    </table>
                    </div>

                </div>
            </div>
        </div> <!-- end col -->
    </div>

    {{-- model --}}

@endsection
@section('scripts')

        <script>
            (function(document) {
    'use strict';

    var LightTableFilter = (function(Arr) {

        var _input;

        function _onInputEvent(e) {
            _input = e.target;
            var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
            Arr.forEach.call(tables, function(table) {
                Arr.forEach.call(table.tBodies, function(tbody) {
                    Arr.forEach.call(tbody.rows, _filter);
                });
            });
        }

        function _filter(row) {
            var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
            row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
        }

        return {
            init: function() {
                var inputs = document.getElementsByClassName('light-table-filter');
                Arr.forEach.call(inputs, function(input) {
                    input.oninput = _onInputEvent;
                });
            }
        };
    })(Array.prototype);

    document.addEventListener('readystatechange', function() {
        if (document.readyState === 'complete') {
            LightTableFilter.init();
        }
    });

})(document);
        </script>
        
@endsection