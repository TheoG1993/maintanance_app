@extends('layouts.master')
@section('styles')

@endsection

@section('body')
    <div class="row">
        <div class="col-12">
            <div class="card m-b-20">
                <div class="card-block">

                    <h4 class="mt-0 header-title">
                        <center>GENERATE MAINTANANCE REPORT</center>
                    </h4>
                
                  
                    <hr>
                    <div class="row">
                    	<div class="col-sm-2">
                    		<h6>Filter By</h6>
                    	</div>
                    	<div class="col-sm-10">
                    		<form class="form-inline" action="{{url('filter/')}}" method="post">
							  <div class="form-group">
							    <label  for="category">Category:</label> &nbsp;
							    <select class="form-control" name="category" id="category">
							    	<option></option>
							    	<option value="1">Computer & Printers</option>
							    	<option value="2">Fire Extinguisher</option>
							    	<option value="3">First Aid Kit</option>
							    	<option value="4">Ac</option>
							    </select>
							  </div> &nbsp;&nbsp;

							  <div class="form-group">
							    <label  for="site">Site:</label>&nbsp;
							    <select class="form-control" id="site" name="site">				<option></option>
							    	        <option value="Site 1">Site 1</option>
						                        <option value="Site 2">Site 2</option>
						                        <option value="Site 3">Site 3</option>
						                        <option value="Site 5">Site 5</option>
						                        <option value="Site 6">Site 6</option>
						                        <option value="Site 7">Site 7</option>
						                        <option value="Site 8">Site 8</option>
						                        <option value="Site 9">Site 9</option>
						                        <option value="Site 10">Site 10</option>
							    </select>
							  </div>&nbsp;
							   
							 &nbsp;&nbsp;
								&nbsp;
							  <button type="submit" class="btn btn-success">GENERATE</button>
							  @csrf
							</form>
                    	</div>
                    	
                    	
                    </div>
                    
                    </div>

                </div>
            </div>
        </div> <!-- end col -->
    </div>


@endsection
@section('scripts')

@endsection