@extends('layouts.master')

@section('body')
	<div class="row">
            <div class="col-md-6 col-lg-6 col-xl-3">
                <a href="{{url('/computers')}}">
                    <div class="mini-stat clearfix bg-primary">
                    <span class="mini-stat-icon"><i class="fa fa-desktop"></i></span>
                    <div class="mini-stat-info text-right text-white">
                        <span class="counter">{{$devices}}</span>
                        Computers ,Printers
                    </div>
                </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
                <a href="{{url('/fire_extinguishers')}}">
                    <div class="mini-stat clearfix bg-primary">
                    <span class="mini-stat-icon"><i class="fa fa-fire-extinguisher"></i></span>
                    <div class="mini-stat-info text-right text-white">
                        <span class="counter">{{$fire_ext}}</span>
                        Fire Extinguisher
                    </div>
                </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
                <a href="{{url('/first_aid_kits')}}">
                    <div class="mini-stat clearfix bg-primary">
                    <span class="mini-stat-icon"><i class="fa fa-medkit"></i></span>
                    <div class="mini-stat-info text-right text-white">
                        <span class="counter">{{$kits}}</span>
                        First Aid Kits
                    </div>
                </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
                <a href="{{url('/generate_report')}}">
                    <div class="mini-stat clearfix bg-primary">
                    <span class="mini-stat-icon"><i class="fa fa-file"></i></span>
                    <div class="mini-stat-info text-right text-white">
                        <span class="counter">{{$acs}}</span>
                        Air Conditioners
                    </div>
                </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
               <center>
                    <img src="{{URL::asset('/assets/images/ofc2.jpg')}}" style="height: 100%; width: 100%;" alt="logo">
               </center>
           </div>
           <div class="col-sm-6">
               <center>
                    <img src="{{URL::asset('/assets/images/ph.jpg')}}" style="height: 100%; width: 100%;" alt="logo">
               </center>
           </div>
        </div>
@endsection