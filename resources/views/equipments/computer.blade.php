@extends('layouts.master')
@section('styles')

@endsection

@section('body')
    <div class="row">
        <div class="col-12">
            <div class="card m-b-20">
                <div class="card-block">

                    <h4 class="mt-0 header-title">
                        <button id="equipment" class="btn btn-success">
                            Add Computer
                        </button>
                    </h4>
                    <input type="search" class="form-control light-table-filter pull-right" data-table="order-table" placeholder="Filter data">
                  

                    <table class="table table-bordered table-hover order-table" style="width: 100%;">
                        <thead>
                        <tr>
                        
                            <th >Description</th>
                            <th>Label</th>
                            <th >Site</th>
                            <th >Last Maintanance</th>
                            <th >Maintanance Date</th>
                            <th >Action</th>

                           
                        </tr>
                        </thead>


                        <tbody>
                       
                        @foreach($devices as $key=>$device)
                            @if($device->status == 1)
                                <tr role="row" class="odd">
                            
                            <td>{{$device->description}}</td>
                            <td>{{$device->label}}</td>
                            <td>{{$device->station}}</td>
                             @if($device->last_maintanance_at == "")
                                <td>No record</td>
                             @else
                                @php 
                                $ldate = Carbon\Carbon::parse($device->last_maintanance_at); 
                               @endphp 
                            <td>{{$ldate->format('M d Y')}}</td>
                             @endif

                            @php 
                                $mdate = Carbon\Carbon::parse($device->maintanance_date); 
                            @endphp 
                            <td>{{$mdate->format('M d Y')}}</td>
                               <td id="actions">
                                <a title="edit" id="device" eqid="{{$device->id}}" description="{{$device->description}}" category="{{$device->category}}" station="{{$device->station}}" style="color: green" >
                                    <span><i class="fa fa-edit"></i></span>
                                </a>
                                |
                                <a title="delete " style="color: black" onclick="function del(){confirm('Are you sure you want to delete this?')};del()" href="{{url('remove/'.$device->id)}}">
                                    <span><i class="fa fa-trash"></i></span>
                                </a>
                            </td>
                           
                        </tr>
                    @elseif($device->status == 0)
                            <tr role="row" class="odd" style="background-color: #FF6666;">
                            
                            <td>{{$device->description}}</td>
                            <td>{{$device->label}}</td>
                            <td>{{$device->station}}</td>
                             @if($device->last_maintanance_at == "")
                                <td>No record</td>
                             @else
                                @php 
                                $ldate = Carbon\Carbon::parse($device->last_maintanance_at); 
                               @endphp 
                            <td>{{$ldate->format('M d Y')}}</td>
                             @endif

                            @php 
                                $mdate = Carbon\Carbon::parse($device->maintanance_date); 
                            @endphp 
                            <td>{{$mdate->format('M d Y')}}</td>
                            <td id="actions">
                                <a title="mark this as maintained" onclick="function del(){confirm('Are you sure?')};del()" href="{{url('update_status/'.$device->id)}}">
                                    <button class="btn btn-xs btn-success ">Mark as maintained</button>
                                </a>
                                |
                                <a title="edit" id="device" eqid="{{$device->id}}" description="{{$device->description}}" category="{{$device->category}}" station="{{$device->station}}" style="color: green" >
                                    <span><i class="fa fa-edit"></i></span>
                                </a>
                                |
                                <a title="delete " style="color: black" onclick="function del(){confirm('Are you sure you want to delete this?')};del()" href="{{url('remove/'.$device->id)}}">
                                    <span><i class="fa fa-trash"></i></span>
                                </a>
                            </td>
                        </tr>
                            @endif
                        @endforeach

                    </tbody>

                    </table>
                    <center>{{$devices->links()}}</center>
                    </div>

                </div>
            </div>
        </div> <!-- end col -->
    </div>

    {{-- model --}}

   <div id="equipmentModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add Computer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Device Description</label>
                        <input type="text" name="description" class="form-control" required placeholder="Description here..">
                    </div>
                    <div class="form-group">
                        <label>Category</label>
                        <select id="category" class="form-control" required >
                            
                                <option value="1">Computer & Printer</option>
                                <option value="2">Fire Extingusher</option>
                                <option value="3">First Aid Kit</option>
                                <option value="4">AC</option>
                            
                        </select>
                    </div>
                     <div class="form-group">
                        <label>Station</label>
                        <select id="station" class="form-control" required >
                            <option value="Site 1">Site 1</option>
                            <option value="Site 2">Site 2</option>
                            <option value="Site 3">Site 3</option>
                            <option value="Site 5">Site 5</option>
                            <option value="Site 6">Site 6</option>
                            <option value="Site 7">Site 7</option>
                            <option value="Site 8">Site 8</option>
                            <option value="Site 9">Site 9</option>
                            <option value="Site 10">Site 10</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="submit" class="btn btn-primary btn-block waves-effect waves-light">Add Computer</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
  </div>

  <div id="equipmentEditModal"  class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Edit Computer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Device Description</label>
                        <input type="text" name="description" id="description" class="form-control" required placeholder="Description here..">
                    </div>
                    <div class="form-group">
                        <label>Category</label>
                        <select id="newcategory" class="form-control" required >
                            
                                <option value="1">Computer & Printer</option>
                                <option value="2">Fire Extingusher</option>
                                <option value="3">First Aid Kit</option>
                                <option value="4">AC</option>
                            
                        </select>
                    </div>
                     <div class="form-group">
                        <label>Station</label>
                        <select id="newstation" name="station" class="form-control" required>
                            <option value="Site 1">Site 1</option>
                            <option value="Site 2">Site 2</option>
                            <option value="Site 3">Site 3</option>
                            <option value="Site 5">Site 5</option>
                            <option value="Site 6">Site 6</option>
                            <option value="Site 7">Site 7</option>
                            <option value="Site 8">Site 8</option>
                            <option value="Site 9">Site 9</option>
                            <option value="Site 10">Site 10</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="editEquipment" class="btn btn-primary btn-block waves-effect waves-light">Edit Computer</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
  </div>
@endsection
@section('scripts')
    
        <script>
            (function(document) {
    'use strict';

    var LightTableFilter = (function(Arr) {

        var _input;

        function _onInputEvent(e) {
            _input = e.target;
            var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
            Arr.forEach.call(tables, function(table) {
                Arr.forEach.call(table.tBodies, function(tbody) {
                    Arr.forEach.call(tbody.rows, _filter);
                });
            });
        }

        function _filter(row) {
            var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
            row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
        }

        return {
            init: function() {
                var inputs = document.getElementsByClassName('light-table-filter');
                Arr.forEach.call(inputs, function(input) {
                    input.oninput = _onInputEvent;
                });
            }
        };
    })(Array.prototype);

    document.addEventListener('readystatechange', function() {
        if (document.readyState === 'complete') {
            LightTableFilter.init();
        }
    });

})(document);
        </script>
        
@endsection