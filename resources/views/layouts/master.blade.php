<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>TRH - MAINTANANCE TRACKER</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf-token" content="{{csrf_token()}}">
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <link href="{{URL::to('assets/plugins/sweet-alert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{URL::to('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{URL::to('assets/css/icons.css')}}" rel="stylesheet" type="text/css">
        <link href="{{URL::to('assets/css/style.css')}}" rel="stylesheet" type="text/css">
        @yield('styles')
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            @include('includes.sidebar_header')
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    @include('includes.header')
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper ">

                        <div class="container">
                            @yield('body')
                            


                        </div><!-- container -->


                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                <footer class="footer">
                 TRH   © @php echo date('Y'); @endphp.
                </footer>

            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="{{URL::to('assets/js/jquery.min.js')}}"></script>
        <script src="{{URL::to('assets/js/tether.min.js')}}"></script><!-- Tether for Bootstrap -->
        <script src="{{URL::to('assets/js/bootstrap.min.js')}}"></script>
        <script src="{{URL::to('assets/js/modernizr.min.js')}}"></script>
        <script src="{{URL::to('assets/js/detect.js')}}"></script>
        <script src="{{URL::to('assets/js/fastclick.js')}}"></script>
        <script src="{{URL::to('assets/js/jquery.slimscroll.js')}}"></script>
        <script src="{{URL::to('assets/js/jquery.blockUI.js')}}"></script>
        <script src="{{URL::to('assets/js/waves.js')}}"></script>
        <script src="{{URL::to('assets/js/jquery.nicescroll.js')}}"></script>
        <script src="{{URL::to('assets/js/jquery.scrollTo.min.js')}}"></script>
       


        <script src="{{URL::to('assets/pages/dashborad.js')}}"></script>

        <!-- App js -->
        <script src="{{URL::to('assets/js/app.js')}}"></script>
        <script src="{{URL::to('assets/js/sweet.js')}}"></script>
        
        
        <script src="{{URL::to('assets/js/scripts.js')}}"></script>

        @yield('scripts')
    </body>
</html>