<div id="sidebar-menu">
                        <ul>
                            <li>
                                <a href="{{url('/dashboard')}}" class="waves-effect">
                                    <i class="mdi mdi-view-dashboard"></i>
                                    <span> Dashboard <span class="badge badge-primary pull-right"></span></span>
                                </a>
                            </li>

                            <li>
                                <a href="{{url('/computers')}}" class="waves-effect">
                                    <i class="mdi mdi-view-dashboard"></i>
                                    <span> Computers <span class="badge badge-primary pull-right"></span></span>
                                </a>
                            </li>

                            <li>
                                <a href="{{url('/fire_extinguishers')}}" class="waves-effect">
                                    <i class="mdi mdi-view-dashboard"></i>
                                    <span> Fire Extinguishers <span class="badge badge-primary pull-right"></span></span>
                                </a>
                            </li>

                            <li>
                                <a href="{{url('/first_aid_kits')}}" class="waves-effect">
                                    <i class="mdi mdi-view-dashboard"></i>
                                    <span> First Aid Kits <span class="badge badge-primary pull-right"></span></span>
                                </a>
                            </li>

                            <li>
                                <a href="{{url('/air_conditions')}}" class="waves-effect">
                                    <i class="mdi mdi-view-dashboard"></i>
                                    <span> AC <span class="badge badge-primary pull-right"></span></span>
                                </a>
                            </li>

                            <li>
                                <a href="{{url('/generate_report')}}" class="waves-effect">
                                    <i class="mdi mdi-view-dashboard"></i>
                                    <span> Report <span class="badge badge-primary pull-right"></span></span>
                                </a>
                            </li>


                        </ul>
                    </div>