<div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="ion-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        
                        <a href="{{url('/dashboard')}}" class="logo"><img src="assets/images/th.jpg" height="70" alt="logo"></a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">

                    <div class="user-details">
                        <div class="user-info">
                            <h4 class="font-16">{{Auth::user()->name}}</h4>
                            
                        </div>
                    </div>

                    @include('includes.sidebar_menu')
                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>